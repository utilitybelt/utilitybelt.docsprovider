﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration.Assemblies;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using UtilityBelt.DocsProvider.models;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider {
    public static class Program {
        internal static Dictionary<ScriptTypeModule, List<ScriptableType>> Modules = new Dictionary<ScriptTypeModule, List<ScriptableType>>();

        public static void Main() {
            try {
                // load assemblies we want to generate docs for
                LoadAssemblies();
                LoadScriptModules();

                foreach (var kv in Modules) {
                    Console.WriteLine($"Module {kv.Key.Name} has {kv.Value.Count} types and {kv.Key.GetRegisteredTypes().Count()} rtypes // {kv.Key.GetScriptTypes().Count()} stypes // {kv.Key.GetExtensions().Count()} etypes");
                    var jsonDocs = new JsonDocs(kv.Key, kv.Value);
                    jsonDocs.Generate(Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), @"docs\json\", $"{kv.Key.Name.Replace(".", "/")}.json"));

                    var luaDefs = new LuaDefs(jsonDocs, kv.Key);
                    luaDefs.Generate(Path.Combine(Path.GetDirectoryName(typeof(Program).Assembly.Location), @"docs\luadefs\", $"{kv.Key.Name.Replace(".", "/")}.lua"));
                }
            }
            catch (Exception ex) {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void LoadScriptModules() {
            foreach (var kv in ScriptableTypes.GetAllModules()) {
                LoadScriptTypes(kv.Key, kv.Value);
            }
        }

        private static void LoadScriptTypes(string moduleName, ScriptTypeModule module) {
            var scriptableTypes = new List<ScriptableType>();
            foreach (var scriptType in module.GetScriptTypes()) {
                if (!IsValidType(scriptType))
                    continue;

                ScriptableType scriptableType = null;
                switch (scriptType.ObjectType) {
                    case ScriptObjectType.Enum:
                        scriptableType = new EnumScriptableType(scriptType);
                        break;

                    case ScriptObjectType.Class:
                        scriptableType = new ClassScriptableType(scriptType);
                        break;

                    case ScriptObjectType.Struct:
                        scriptableType = new ClassScriptableType(scriptType);
                        break;

                    default:
                        break;
                }

                if (scriptableType != null) {
                    scriptableTypes.Add(scriptableType);
                }
                else {
                    Console.WriteLine($"Unhandled ScriptType: {scriptType.ObjectType} // {scriptType.Type}");
                }
            }

            Modules.Add(module, scriptableTypes);
        }

        private static bool IsValidType(ScriptType scriptType) {
            if (scriptType.Type.FullName.Contains("`"))
                return false;
            if (scriptType.Type.FullName.Contains("<"))
                return false;
            if (scriptType.Type.FullName.StartsWith("System"))
                return false;
            if (scriptType.Type.FullName.StartsWith("UtilityBelt.Scripting.ScriptEnvs"))
                return false;
            return true;
        }

        private static void LoadAssemblies() {
            foreach (string scriptTypesFile in Directory.GetFiles(Path.GetDirectoryName(typeof(Program).Assembly.Location), "*.ScriptTypes.json")) {
                var assemblyFile = scriptTypesFile.Replace(".ScriptTypes.json", ".dll");
                try {
                    Assembly.LoadFile(assemblyFile);
                }
                catch (Exception ex) {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
    }
}