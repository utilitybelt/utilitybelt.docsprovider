﻿using ACE.DatLoader;
using Newtonsoft.Json;
using NuDoq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using UtilityBelt.Common.Enums;
using UtilityBelt.DocsProvider.models;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using UtilityBelt.Service.Lib.Settings;
using WattleScript.Interpreter;
using static UtilityBelt.Common.Messages.Types.PhysicsDesc;

namespace UtilityBelt.DocsProvider {
    internal class LuaDefs {
        private JsonDocs jsonDocs;

        private static List<string> reservedWords = new List<string>() {
            "and", "break", "do", "else", "elseif", "end", "false", "for", "function", "if", "in", "local", "nil", "not", "or", "repeat", "return", "then", "true", "until", "while"
        };


        /// <summary>
        /// Maps CLR type to what will be used in Lua
        /// </summary>
        private static Dictionary<string, string> luaTypes = new Dictionary<string, string>() {
            {            "System.Void" , ""},
{            "MoonSharp.Interpreter.DynValue" , "*"},
{            "System.SByte" , "number"},
{            "System.Byte" , "number"},
{            "System.Int16" , "number"},
{            "System.UInt16" , "number"},
{            "System.Int32" , "number"},
{            "System.UInt32" , "number"},
{            "System.Int64" , "number"},
{            "System.UInt64" , "number"},
{            "System.Single" , "number"},
{            "System.Decimal" , "number"},
{            "System.Double" , "number"},
{            "System.Boolean" , "boolean"},
{            "System.String" , "string"},
{            "System.Text.StringBuilder" , "string"},
{            "System.Char" , "string"},
            //TODO -- these probably require different mapping
{            "MoonSharp.Interpreter.Table" , "table"},
{            "MoonSharp.Interpreter.CallbackFunction" , "function"},
{            "System.Delegate" , "function"},
{            "System.Object" , "userdata"},
{            "System.Type" , "userdata"},
{            "MoonSharp.Interpreter.Closure" , "function"},
{            "System.Reflection.MethodInfo" , "function"},
{            "System.Collections.IList" , "table"},
{            "System.Collections.IDictionary" , "table"},
{            "System.Collections.IEnumerable" , "iterator"},
{            "System.Collections.IEnumerator" , "iterator"},
            };

        public bool IsBaseModule { get; private set; }
        public ScriptTypeModule Module { get; }

        public LuaDefs(JsonDocs jsonDocs, ScriptTypeModule module) {
            this.jsonDocs = jsonDocs;
            Module = module;
        }

        internal void Generate(string outFile) {
            if (!Directory.Exists(Path.GetDirectoryName(outFile))) {
                Directory.CreateDirectory(Path.GetDirectoryName(outFile));
            }


            var str = new StringBuilder();
            str.AppendLine($"---@meta");
            str.AppendLine($"local __localDefs = {{}}");
            str.AppendLine($"local __localTypeDefs = {{}}");

            if (outFile.EndsWith("base.lua")) {
                IsBaseModule = true;
                str.AppendLine(@"

---@alias IntPtr integer

---@class DateTime: Object
---@class TimeSpan: Object
---@class EventArgs: Object

---@class Dictionary<K,V>: { [K]: V }
---@class IDictionary<K,V>: { [K]: V }
---@class Array<T>: { [integer]: T }
---@class List<T>: { [integer]: T }
---@class IList<T>: { [integer]: T }
---@class Queue<T>: { [integer]: T }
---@class ObservableCollection<T>: { [integer]: T }
---@class IEnumerable<T>: { [integer]: T }
---@class Nullable<T>: { Value: T, HasValue: fun(): boolean }

---@class Vector2" + "\u200e" + @"
Vector2 = {};

--- Create a new Vector2 instance
---@param x integer x value
---@param y integer y value
---@return Vector2 a new Vector2 instance
function Vector2.new(x, y) end

---@class Vector2: Object
---@field X integer
---@field Y integer
local Vector2_Instance = {}

---@class Vector3" + "\u200e" + @"
Vector3 = {};

--- Create a new Vector3 instance
---@param x integer x value
---@param y integer y value
---@param z integer z value
---@return Vector3 a new Vector3 instance
function Vector3.new(x, y, z) end

---@class Vector3: Object
---@field X integer
---@field Y integer
---@field Z integer
local Vector3_Instance = {}

---@class Vector4" + "\u200e" + @"
Vector4 = {};

--- Create a new Vector4 instance
---@param x integer x value
---@param y integer y value
---@param z integer z value
---@param w integer 2 value
---@return Vector4 a new Vector4 instance
function Vector4.new(x, y, z, w) end

---@class Vector4: Object
---@field X integer
---@field Y integer
---@field Z integer
---@field W integer
local Vector4_Instance = {}

---@class Event<T>: { Add: fun(handler: fun(evt: T)), Remove: fun(handler: fun(evt: T)), Once: fun(handler: fun(evt: T)), Until: fun(handler: fun(evt: T)) }
---@class Event: { Add: fun(handler: fun()), Remove: fun(handler: fun()), Once: fun(handler: fun()), Until: fun(handler: fun()) }

---@class EnumConst: integer
local EnumConst = {}

---Check if an enum value has the specified enum flags
---@param flags EnumConst flags to check for
---@return boolean -- true if has flags
function EnumConst.HasFlags(flags) end

---Add the specified flags to an enum, using binary 'or' operator
---@param flags EnumConst flags to add
---@return EnumConst
function EnumConst.AddFlags(flags) end

---Remove the specified flags from an enum, using binary 'and not' operator
---@param flags EnumConst flags to remove
---@return EnumConst
function EnumConst.RemoveFlags(flags) end

---Convert EnumConst to a number type
---@return EnumConst
function EnumConst.ToNumber() end

---@class Object
local Object = {}

---Returns a list of strings of all public property / field names. Useful for reflection on UserData in lua scripts
function Object.GetPropertyKeys() end

---@class JSON
json = {}

---Deserialize json text to a table
---@param text string -- json text to deserialize
---@return table
function json.parse(text) end

---Serialize a table to a json string
---@param t table -- table to serialize
---@return string
function json.serialize(t) end

---Block the current lua chunk from executing until the passed action is resolved
---@param action QueueAction --The action to wait for
---@return QueueAction --The action
function await(action) end

---Block the current lua chunk from executing for the specified milliseconds
---@param milliseconds number -- The number of milliseconds to block execution for
function sleep(milliseconds) end

---@type Game
game = {};
                    ");
            }
            else {
                str.AppendLine("local _module = {}");

                foreach (var instance in Module.GetInstances()) {
                    str.AppendLine($"---@type {instance.Value.Name}");
                    str.AppendLine($"_module.{instance.Key} = {{}}");
                }
            }

            foreach (var type in jsonDocs.Types) {
                switch (type.ScriptType.ObjectType) {
                    case Scripting.Lib.ScriptTypes.ScriptObjectType.Enum:
                        WriteEnum(str, type as EnumScriptableType);
                        break;
                    case Scripting.Lib.ScriptTypes.ScriptObjectType.Class:
                        WriteClass(str, type as ClassScriptableType);
                        break;
                    case Scripting.Lib.ScriptTypes.ScriptObjectType.Struct:
                        WriteClass(str, type as ClassScriptableType);
                        break;
                }
            }

            if (!IsBaseModule) {
                if (Module.ModuleReturnType != null) {
                    str.AppendLine($"return __localDefs.{Module.ModuleReturnType.Name};");
                }
                else {
                    str.AppendLine("return _module;");
                }
            }

            File.WriteAllText(outFile, str.ToString());
        }

        private void WriteClass(StringBuilder output, ClassScriptableType type) {
            var constructors = type.Children.Where(t => t.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Constructor);
            var staticMembers = type.Children.Where(t => {
                if (t.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Method && (t.ScriptType.MemberInfo as MethodInfo).IsStatic)
                    return true;

                if (t.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Property && (t.ScriptType.MemberInfo as PropertyInfo).GetAccessors().FirstOrDefault()?.IsStatic == true)
                    return true;

                if (t.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Field && (t.ScriptType.MemberInfo as FieldInfo).IsStatic)
                    return true;

                return false;
            });
            var instanceMembers = type.Children.Where(t => !staticMembers.Contains(t));

            // class type definition, only provided if there are public constructors / static methods.
            if (constructors.Count() > 0 || staticMembers.Count() > 0) {
                output.AppendLine(FormatComment(type.Summary));
                output.AppendLine($"---@class {type.Name}\u200e");
                foreach (var staticField in staticMembers.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Field || s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Property)) {
                    output.AppendLine($"---@field {staticField.Name} {FormatType(staticField.ScriptType.Type)} {FormatComment(staticField.Summary)}");
                }

                if (type.ScriptType.IsGlobal) {
                    var prefix = type.ScriptType.IsGlobal ? (IsBaseModule ? "" : "_module.") : "__localTypeDefs.";

                    output.AppendLine($"{prefix}{type.Name} = {{}}");
                    output.AppendLine();

                    foreach (var constructorMethod in constructors.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Constructor)) {
                        WriteMethod(output, constructorMethod, $"{prefix}{constructorMethod.ScriptType.ParentType.Name}");
                    }
                    foreach (var staticMethod in staticMembers.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Method)) {
                        WriteMethod(output, staticMethod, $"{prefix}{staticMethod.ScriptType.ParentType.Name}");
                    }
                }
            }

            output.AppendLine(FormatComment(type.Summary));
            // todo: proper inheritance
            output.AppendLine($"---@class {type.Name}: {(typeof(QueueAction).IsAssignableFrom(type.ScriptType.Type) ? "QueueAction" : "Object")}");

            var tableChildren = new List<ScriptableType>();
            foreach (var child in instanceMembers.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Field || s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Property)) {
                if (IsList(child.ScriptType.Type) || IsDictionary(child.ScriptType.Type)) {
                    tableChildren.Add(child);
                }
                else {
                    output.AppendLine($"---@field {child.Name} {FormatType(child.ScriptType.Type)} {FormatComment(child.Summary)}");
                }
            }
            foreach (var child in instanceMembers.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Event)) {
                if (child.ScriptType.Type.IsGenericType) {
                    output.AppendLine($"---@field {child.Name} Event<{FormatType(child.ScriptType.Type.GetGenericArguments().First())}> {FormatComment(child.Summary)}");
                }
                else {
                    output.AppendLine($"---@field {child.Name} Event {FormatComment(child.Summary)}");
                }
            }

            output.AppendLine($"__localDefs.{type.Name} = {{}}");
            output.AppendLine();

            foreach (var child in tableChildren) {
                output.AppendLine($"-- {FormatComment(child.Summary)}");
                output.AppendLine($"__localDefs.{type.Name}.{child.Name} = {{}} ---@type {FormatType(child.ScriptType.Type)}");
                output.AppendLine();
            }

            foreach (var method in instanceMembers.Where(s => s.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Method)) {
                WriteMethod(output, method, $"__localDefs.{method.ScriptType.ParentType.Name}");
            }

            //WriteExtensionMethods(output, type, $"__localDefs.{type.ScriptType.Type.Name}");
        }

        private void WriteExtensionMethods(StringBuilder output, ClassScriptableType type, string parentClass) {
            var extensionMethods = GetExtensionMethodsForType(type);

            foreach (var methodInfo in extensionMethods) {
                output.AppendLine(FormatComment($"{methodInfo.DeclaringType.Namespace}.{methodInfo.DeclaringType.Name} // {methodInfo.Name}"));
                foreach (var param in methodInfo.GetParameters().Skip(1)) {
                    var paramComment = "param comment...";
                    output.AppendLine($"---@param {FixParamName(param.Name)}{(param.IsOptional ? "?" : "")} {FormatType(param.ParameterType)} {FormatComment(paramComment)}");
                }
                var returnComment = "return comment...";
                output.AppendLine($"---@return {FormatType(methodInfo.ReturnType)} {FormatComment(returnComment)}");
                output.AppendLine($"function {parentClass}.{methodInfo.Name}({string.Join(", ", methodInfo.GetParameters().Skip(1).Select(p => FixParamName(p.Name)))}) end");
                output.AppendLine();
            }
        }

        private IEnumerable<MethodInfo> GetExtensionMethodsForType(ClassScriptableType type) {
            var extensionMethods = new List<MethodInfo>();
            var extensionClasses = new List<Type>();
            if (Module.Name.ToLower() != "base") {
                extensionClasses.AddRange(ScriptableTypes.GetModule("base").GetExtensions());
            }
            extensionClasses.AddRange(Module.GetExtensions());

            foreach (var extClass in extensionClasses) {
                Console.WriteLine($"Found extension class: {extClass.Namespace}.{extClass.Name}");
                foreach (var methodInfo in extClass.GetMethods(BindingFlags.Static | BindingFlags.Public)) {
                    if (methodInfo.GetParameters().Count() == 0)
                        continue;
                    var typeToExtend = methodInfo.GetParameters().First().ParameterType;
                    if (typeToExtend.IsAssignableFrom(type.ScriptType.Type)) {
                        extensionMethods.Add(methodInfo);
                    }
                }
            }

            return extensionMethods;
        }

        private void WriteMethod(StringBuilder output, ScriptableType methodType, string parentClass) {
            var methodInfo = methodType.ScriptType.MemberInfo as MethodInfo;
            var methodName = methodType.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Constructor ? "new" : methodInfo.Name;
            output.AppendLine(FormatComment(methodType.Summary));
            if (methodType.ScriptType.ObjectType == Scripting.Lib.ScriptTypes.ScriptObjectType.Constructor) {
                var constructorInfo = (methodType.ScriptType.MemberInfo as ConstructorInfo);
                foreach (var param in constructorInfo.GetParameters()) {
                    var paramComment = "";
                    if (methodType.ScriptType.Docs?.Params.TryGetValue(param.Name, out string v) == true) {
                        paramComment = v;
                    }
                    output.AppendLine($"---@param {FixParamName(param.Name)}{(param.IsOptional ? "?" : "")} {FormatType(param.ParameterType)} {FormatComment(paramComment)}");
                }
                output.AppendLine($"---@return {FormatType(methodType.ScriptType.ParentType)} -- A new instance of {FormatType(methodType.ScriptType.ParentType)}");
                output.AppendLine($"function {parentClass}.{methodName}({string.Join(", ", constructorInfo.GetParameters().Select(p => FixParamName(p.Name)))}) end");
            }
            else {
                var hasErrorRef = false;
                foreach (var param in methodInfo.GetParameters()) {
                    var paramComment = "";
                    if (methodType.ScriptType.Docs?.Params.TryGetValue(param.Name, out string v) == true) {
                        paramComment = v;
                    }

                    if (param.ParameterType.IsByRef && param.Name == "error") {
                        hasErrorRef = true;
                    }
                    else {
                        output.AppendLine($"---@param {FixParamName(param.Name)}{(param.IsOptional ? "?" : "")} {FormatType(param.ParameterType)} {FormatComment(paramComment)}");
                    }
                }
                var returnComment = "";
                if (!string.IsNullOrEmpty(methodType.ScriptType.Docs?.Returns)) {
                    returnComment = methodType.ScriptType.Docs.Returns;
                }
                output.AppendLine($"---@return {FormatType(methodInfo.ReturnType)} {FormatComment(returnComment)}");
                if (hasErrorRef) {
                    output.AppendLine($"---@return string -- the error, if any");
                    output.AppendLine($"function {parentClass}.{methodName}({string.Join(", ", methodInfo.GetParameters().Select(p => FixParamName(p.Name)).Take(methodInfo.GetParameters().Length - 1))}) end");
                }
                else {
                    output.AppendLine($"function {parentClass}.{methodName}({string.Join(", ", methodInfo.GetParameters().Select(p => FixParamName(p.Name)))}) end");
                }
            }
            output.AppendLine();
        }

        private string FixParamName(string name) {
            if (reservedWords.Contains(name.ToLower()))
                return $"{name}_";
            return name;
        }

        private void WriteEnum(StringBuilder output, EnumScriptableType type) {
            output.AppendLine($"---@class {type.Name}: EnumConst");
            output.AppendLine($"-{FormatComment(type.Summary)}");
            output.AppendLine($"---@class {type.Name}\u200e");

            foreach (var value in type.Fields) {
                if (type.IsFlags) {
                    output.AppendLine($"---@field {value.Name} {type.Name} {FormatComment(value.Summary)}");
                }
                else {
                    output.AppendLine($"---@field {value.Name} {type.Name} {FormatComment(value.Summary)}");
                }
            }
            output.AppendLine($"---@type {type.Name}\u200e");
            if (IsBaseModule) {
                output.AppendLine($"{type.Name} = {{ }}");
            }
            else {
                output.AppendLine($"_module.{type.Name} = {{ }}");
            }
            output.AppendLine();
        }

        private string FormatComment(string comment) {
            if (string.IsNullOrEmpty(comment))
                return string.Empty;
            return $"-- {comment.Replace("\n", "<br />").Replace("\r", "")}";
        }

        public static string FormatType(Type type) {
            if (type == null || type == typeof(void)) {
                return "nil";
            }
            if (type == typeof(Closure)) {
                return $"fun(...):nil";
            }
            if (type.IsGenericType || type.GetGenericArguments().Length > 0) {
                // todo: this is no good
                if (type.Name.Split('`').First().Equals("Action")) {
                    if (type.GetGenericArguments().Length == 2 && type.GetGenericArguments().All(a => a == typeof(string))) {
                        return $"fun(channel: string, message: string): nil";
                    }
                    return $"fun(res: {FormatType(type.GetGenericArguments().First())}): nil";
                }
                if (type.Name.Split('`').First().Equals("Func")) {
                    return $"fun(res: {FormatType(type.GetGenericArguments().First())}): nil";
                }
                if (IsList(type)) {
                    return $"{{ [number]: {FormatType(type.GetGenericArguments().First())} }}";
                }
                if (IsDictionary(type)) {
                    return $"{{ [{FormatType(type.GetGenericArguments().First())}]: {FormatType(type.GetGenericArguments().Last())} }}";
                }

                var typeStr = ConvertTypeName($"{type.Namespace}.{type.Name.Split('`').First()}");
                var genericTypeArgs = type.GetGenericArguments().Select(a => FormatType(a));

                return $"{typeStr}<{string.Join(", ", genericTypeArgs)}>";
            }

            return ConvertTypeName($"{type.Namespace}.{type.Name.Split('`').First().Split('&').First().Split('*').First()}");
        }

        public static bool IsList(Type type) {
            return type.Name.Split('`').First().Equals("List") || type.Name.Split('`').First().Equals("IList");
        }

        public static bool IsDictionary(Type type) {
            return type.Name.Split('`').First().Equals("Dictionary") || type.Name.Split('`').First().Equals("IDictionary");
        }

        public static string ConvertTypeName(string type) {
            if (luaTypes.TryGetValue(type, out string val)) {
                return val;
            }

            return type.Split('.').Last();
        }
    }
}