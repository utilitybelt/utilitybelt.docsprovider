﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.DocsProvider.models {
    public class ReturnType {
        public string Type { get; set; }
        public string Summary { get; set; }
    }
}
