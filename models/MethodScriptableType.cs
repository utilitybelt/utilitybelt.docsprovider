﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider.models {
    internal class MethodScriptableType : ScriptableType {
        public List<MethodParam> Params { get; } = new List<MethodParam>();

        public ReturnType Returns { get; set; }

        public MethodScriptableType(ScriptType type) : base(type) {
            var docs = type.Docs;

            if (type.MemberInfo is MethodInfo methodInfo) {
                foreach (var paramInfo in methodInfo.GetParameters()) {
                    Params.Add(FromParamInfo(paramInfo));
                }
                Returns = new ReturnType() {
                    Type = FormatType(methodInfo.ReturnType),
                    Summary = string.IsNullOrEmpty(docs.Returns)? "TODO" : docs.Returns
                };
            }
            else if (type.MemberInfo is ConstructorInfo constructorInfo) {
                foreach (var paramInfo in constructorInfo.GetParameters()) {
                    Params.Add(FromParamInfo(paramInfo));
                }
                Returns = new ReturnType() {
                    Type = FormatType(type.ParentType),
                    Summary = string.IsNullOrEmpty(docs.Returns) ? "TODO" : docs.Returns
                };
            }
        }

        private MethodParam FromParamInfo(ParameterInfo paramInfo) {
            return new MethodParam() {
                Name = paramInfo.Name,
                HasDefault = paramInfo.HasDefaultValue,
                DefaultValue = paramInfo.DefaultValue,
                IsOut = paramInfo.IsOut,
                IsRef = paramInfo.ParameterType.IsByRef,
                Summary = ScriptType.Docs.Params.ContainsKey(paramInfo.Name) ? ScriptType.Docs.Params[paramInfo.Name] : "TODO",
                Type = FormatType(paramInfo.ParameterType)
            };
        }
    }
}
