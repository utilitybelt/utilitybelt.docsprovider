﻿using NuDoq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider.models {
    internal class EnumScriptableType : ScriptableType {
        public string ValueType => ScriptType.Type.GetEnumUnderlyingType().FullName;

        public List<EnumField> Fields { get; } = new List<EnumField>();

        public bool IsFlags => ScriptType.MemberInfo.GetCustomAttributes(true).Any(t => t is FlagsAttribute);

        public EnumScriptableType(ScriptType type) : base(type) {
            //type.LoadDocumentation();
            foreach (var child in ScriptType.Children) {
                Fields.Add(new EnumField() {
                    Name = child.Name,
                    Value = Convert.ChangeType(System.Enum.Parse(type.Type, child.Name), System.Enum.GetUnderlyingType(type.Type)),
                    Summary = child.Docs?.Summary ?? ""
                });
            }
        }
    }
}
