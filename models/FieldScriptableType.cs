﻿using NuDoq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using static UtilityBelt.Common.Messages.Types.PhysicsDesc;

namespace UtilityBelt.DocsProvider.models {
    internal class FieldScriptableType : ScriptableType {

        public FieldScriptableType(ScriptType type) : base(type) {
            //type.LoadDocumentation();
        }
    }
}
