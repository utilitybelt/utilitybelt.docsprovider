﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider.models {
    internal class ClassScriptableType : ScriptableType {
        public bool IsStatic => ScriptType.Type.IsAbstract && ScriptType.Type.IsSealed;

        public List<ScriptableType> Children = new List<ScriptableType>();

        public bool HasDefinedConstructor = false;

        public ClassScriptableType(ScriptType type) : base(type) {
            //type.LoadDocumentation();

            HasDefinedConstructor = ScriptType.Type.GetConstructors(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Length > 0 || ScriptType.Type.GetConstructors(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance).Length <= 0;

            foreach (var child in type.Children) {
                switch (child.ObjectType) {
                    case ScriptObjectType.Field:
                        Children.Add(new FieldScriptableType(child));
                        break;
                    case ScriptObjectType.Property:
                        Children.Add(new PropertyScriptableType(child));
                        break;
                    case ScriptObjectType.Method:
                        Children.Add(new MethodScriptableType(child));
                        break;
                    case ScriptObjectType.Constructor:
                        Children.Add(new MethodScriptableType(child));
                        break;
                    case ScriptObjectType.Event:
                        Children.Add(new EventScriptableType(child));
                        break;
                }
            }
        }
    }
}
