﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.DocsProvider.models {
    internal class EnumField {
        public string Name { get; set; }

        public object Value { get; set; }

        public string Summary { get; set; }
    }
}
