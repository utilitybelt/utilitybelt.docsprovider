﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.DocsProvider.models {
    public class MethodParam {
        public string Name { get; set; }

        public bool HasDefault { get; set; }

        public object DefaultValue { get; set; }

        public string Summary { get; set; }

        public bool IsRef { get; set; }

        public bool IsOut { get; set; }
        public string Type { get; set; }
    }
}
