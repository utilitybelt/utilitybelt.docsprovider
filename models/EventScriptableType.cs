﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider.models {
    internal class EventScriptableType : ScriptableType {

        public EventScriptableType(ScriptType type) : base(type) {
            //type.LoadDocumentation();
        }
    }
}
