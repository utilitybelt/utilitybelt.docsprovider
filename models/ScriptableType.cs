﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Lib;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider.models {
    internal abstract class ScriptableType {

        [JsonIgnore]
        public ScriptType ScriptType { get; }

        public string Name => ScriptType.Name;
        public string Module { get; }

        public string Type { get; }
        public string Id { get; protected set; } = null;

        public string ObjectType => ScriptType.ObjectType.ToString();

        public string Summary => string.IsNullOrEmpty(ScriptType.Docs?.Summary) ? $"TODO: docs" : ScriptType.Docs.Summary;

        public ScriptableType(ScriptType type) {
            Id = type.Id;
            ScriptType = type;
            Type = FormatType(type.Type);
            Module = type.Module.Name;
        }

        public static string FormatType(Type type) {
            if (type.IsGenericType) {
                var typeStr = $"{type.Namespace}.{type.Name.Split('`').First()}";
                var genericTypeArgs = type.GetGenericArguments().Select(a => FormatType(a));

                return $"{typeStr}<{string.Join(", ", genericTypeArgs)}>";
            }

            return $"{type.Namespace}.{type.Name.Replace("&", "")}";
        }
    }
}
