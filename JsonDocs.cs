﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.DocsProvider.models;
using UtilityBelt.Scripting.Lib.ScriptTypes;

namespace UtilityBelt.DocsProvider {
    internal class JsonDocs {
        public string Summary { get; }
        public string Exports { get; }

        public IEnumerable<ScriptableType> Types { get; }

        public JsonDocs(ScriptTypeModule module, IEnumerable<ScriptableType> types) {
            Summary = module.Description;
            Types = types;

            Exports = module.GetScriptTypes().Where(t => t.Type == module.ModuleReturnType).FirstOrDefault()?.Id;
        }

        internal void Generate(string outFile) {
            if (!Directory.Exists(Path.GetDirectoryName(outFile))) {
                Directory.CreateDirectory(Path.GetDirectoryName(outFile));
            }

            File.WriteAllText(outFile, JsonConvert.SerializeObject(this, Formatting.Indented));
        }
    }
}
